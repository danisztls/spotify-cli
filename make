#!/bin/bash
# Setup a server and a console client to play Spotify on Arch Linux

install() {
    # Install dependencies
    sudo pacman -S --needed mopidy ncmpcpp
    yay -S --needed --batchinstall mopidy-spotify mopidy-mpd mopidy-local

    # Copy configuration files
    if [ -f mopidy.conf ]; then
        mkdir -p "$HOME/.config/mopidy" && cp mopidy.conf "$HOME/.config/mopidy/mopidy.conf"
    else
        echo "mopidy.conf not found"
    fi

    if [ -f ncmpcpp.conf ]; then
        mkdir -p "$HOME/.config/ncmpcpp" && cp ncmpcpp.conf "$HOME/.config/ncmpcpp/config"
    else
        echo "ncmpcpp.conf not found"
    fi

    # Copy systemd files
    mkdir -p "$HOME/.config/systemd/user" && cp mopidy.service "$HOME/.config/systemd/user/mopidy.service"

    # update local media library
    mopidy local scan 

    # Start and enable mopidy service
    systemctl --user daemon-reload && systemctl --user start mopidy && systemctl --user enable mopidy

    # Install notification script
    cp notify-song "$HOME/.local/bin/"
}

uninstall() {
    sudo pacman -Rcs mopidy ncmpcpp mopidy-spotify mopidy-mpd mopidy-local
    [ -f "$HOME/.local/bin/notify-song" ] && rm "$HOME/.local/bin/notify-song"
    systemctl --user stop mopidy && systemctl --user disable mopidy
    [ -f "$HOME/.config/systemd/user/mopidy.service" ] && rm "$HOME/.config/systemd/user/mopidy.service"
    #[ -f "$HOME/.config/ncmpcpp/config" ] && rm "$HOME/.config/ncmpcpp/config"
    #[ -f "$HOME/.config/mopidy/mopidy.conf" ] && rm "$HOME/.config/mopidy/mopidy.conf"
}

case $1 in
    install) install;;
    uninstall) uninstall;;
esac

