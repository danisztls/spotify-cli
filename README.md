<!-- TOC GitLab -->

- [Setup](#setup)
- [Server](#server)
- [Client](#client)
- [Misc](#misc)
    - [Media keymaps](#media-keymaps)
    - [Local library](#local-library)

<!-- /TOC -->

**mopidy** is a daemon somewhat similar to MPD with plugins for Spotify, SoundCloud, Youtube and others. **ncmpcpp** is a console client for MPD. **spotify-cli** is a set of configuration files, a service and a script to easily setup these tools and play Spotify songs via console in Arch and other distros.

# Setup
Generate client and secret id in [Mopidy-Spotify](https://mopidy.com/ext/spotify/).

Configure mopidy-template.conf with your spotify username, password, client id and secret id and rename it to mopidy.conf.

Install yay, configure another AUR helper or install packages manually and then run *make install*. To uninstall run *make uninstall*.

# Server
The mopidy server can be managed through systemctl.

`systemctl --user enable mopidy && systemcl --user start mopidy`

# Client
F1 displays ncmpcpp's keyboard shortcuts and you can find a list of them [here](https://pkgbuild.com/~jelle/ncmpcpp).

To enable visualization uncomment the configuration bellow on *mopidy.conf*. It isn't enabled by default because with it enable playback will crash when resuming a song after a pause and also visualizations get desynced all the time with Spotify songs.

```
[audio]
output = tee name=t t. ! queue ! autoaudiosink t. ! queue ! audioresample ! audioconvert ! audio/x-raw,rate=44100,channels=2,format=S16LE ! wavenc ! filesink location=/tmp/mpd.fifo
```

An alternative to having no visualization is to have a fake random one. And for that you can uncomment the code bellow on *ncmpcpp.conf*.

```
visualizer_fifo_path = "/dev/random"
```

If you want to open the playlist on the left side and visualizer on the right side you use this alias:

```shell
alias 'ncm=ncmpcpp -s playlist -S visualizer'
```

![ncmpcpp screenshoot](ncmpcpp.png)

# Misc
## Media keymaps
To control Mopidy with keyboard shortcuts you can install mpc and assign shortcuts to it in your desktop environment settings.

What I use:
- alt + p to trigger `mpc toggle` which will trigger play/pause.
- alt + [ to trigger `mpc prev` which will play previous song.
- alt + ] to trigger `mpc next` which will play next song.
- alt + | to trigger `notify-send` and `mpc current` which will display a notification of current song through the command bellow.

```shell
notify-send "Now playing" "$(mpc current)"'
```

Unfortunately GNOME stopped supporting aliases and don't support command substitution. So I created a small script, *notify-song* , just for that.

## Local library
Default dir is `$XDG_MUSIC_DIR`. It have to be scanned before the songs will be displayed in the client. That is done by running `mopidy local scan` which the setup script will do automatically.   

